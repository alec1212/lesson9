package com.galkin.jms;

import com.galkin.jms.libs.Chat;
import com.galkin.jms.service.PropertyService;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;

import javax.jms.*;
import java.util.Scanner;


public class App {


    @SneakyThrows
    public static void main(String[] args) {

        PropertyService propertyService = new PropertyService();

        if (!propertyService.getExternalServer()) {

            System.out.println("Embeded server is using");
            BasicConfigurator.configure();
            final BrokerService broker = new BrokerService();
            broker.addConnector("tcp://localhost:61616");
            broker.start();
        }

        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to chat!!");
        System.out.println("Broadcast - bc | Private - pc | exit - quit");
        System.out.println("");
        System.out.println("Please input your login:");
        String login = scanner.nextLine();
        while (login.isEmpty()) {
            System.out.println("Please input your login:");
            login = scanner.nextLine();

        }

        Chat chat = new Chat(login);
        chat.init();

        System.out.println("Please enter command:");
        String command = "";

        while (!"exit".equals(command)) {
            System.out.println(command);

            command = scanner.nextLine();

            if ("bc".equals(command)) {
                System.out.println("Enter message:");
                String message = scanner.nextLine();
                chat.sendBroadcast(message);
                continue;
            }

            if ("pc".equals(command)) {
                System.out.println("Enter login:");
                String loginTo = scanner.nextLine();
                System.out.println("Enter message:");
                String message = scanner.nextLine();
                chat.sendPrivate(loginTo, message);
                continue;
            }
        }
        System.exit(0);
    }
}
