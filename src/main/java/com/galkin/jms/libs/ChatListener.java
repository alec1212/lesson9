package com.galkin.jms.libs;

import lombok.SneakyThrows;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

public class ChatListener implements MessageListener {


    @SneakyThrows
    @Override
    public void onMessage(Message message) {

        if (message instanceof ObjectMessage){
            ObjectMessage objectMessage = (ObjectMessage)message;
            System.out.println(String.format("%s", objectMessage.getObject()));

        }
        System.out.println();
    }
}
