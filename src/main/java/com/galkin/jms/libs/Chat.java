package com.galkin.jms.libs;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.Arrays;

@Getter
@Setter
public class Chat {

    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;
    private static final String BROADCAST = "ALL";
    private static final String PRIVATE = "PRIVATE";

    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destinationAll;
    private MessageProducer messageProducerAll;
    private MessageConsumer messageConsumerAll;

    private Destination destinationPrivate;
    private MessageConsumer messageConsumerPrivate;
    private String login;

    public Chat(String login) {
        this.login = login;
    }

    @SneakyThrows
    public void init()  {


        connectionFactory =  new ActiveMQConnectionFactory(URL);
        connectionFactory.setTrustedPackages(Arrays.asList("com.galkin.jms.libs"));
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        final ChatListener chatListener = new ChatListener();

        destinationAll = session.createTopic(BROADCAST);

        messageProducerAll = session.createProducer(destinationAll);
        messageConsumerAll = session.createConsumer(destinationAll);
        messageConsumerAll.setMessageListener(chatListener);

        destinationPrivate = session.createQueue(String.format("Private%s", login));

        messageConsumerPrivate = session.createConsumer(destinationPrivate);
        messageConsumerPrivate.setMessageListener(chatListener);

    }

    @SneakyThrows
    public void  sendBroadcast(String message)  {
        if (message == null || message.isEmpty()) return;

        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setLoginFrom(login);
        chatMessage.setText(message);
        messageProducerAll.send(session.createObjectMessage(chatMessage));

    }

    @SneakyThrows
    public void  sendPrivate(String loginTo, String message)  {
        if (message == null || message.isEmpty()) return;
        if (loginTo == null || loginTo.isEmpty()) return;

        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setLoginFrom(login);
        chatMessage.setText(message);
        Destination destinationPrivateTo = session.createQueue(String.format("Private%s", loginTo));
        MessageProducer messageProducerPrivate = session.createProducer(destinationPrivateTo);
        messageProducerPrivate.send(session.createObjectMessage(chatMessage));

    }
}
