package com.galkin.jms.libs;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ChatMessage implements Serializable {

    private String text;
    private String loginFrom;

    @Override
    public String toString() {
        return String.format("%s : %s", loginFrom, text);
    }
}
