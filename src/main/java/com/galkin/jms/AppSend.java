package com.galkin.jms;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.BasicConfigurator;

import javax.jms.*;

/**
 * Hello world!
 *
 */
public class AppSend
{
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;
    private static final String TOPIC = "MY_TOPIC";

    public static void main( String[] args ) throws JMSException {
        BasicConfigurator.configure();
        final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        final Destination destination = session.createTopic(TOPIC);
        final MessageProducer messageProducer = session.createProducer(destination);
        final TextMessage  textMessage = session.createTextMessage("Hello from A. Galkin");
        messageProducer.send(textMessage);
        System.out.println(String.format("Отправлено: %s", textMessage.getText()));
        connection.close();


    }
}
