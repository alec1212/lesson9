package com.galkin.jms.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public  class PropertyService {

    private static final String FILE = "/application.properties";


    private static final InputStream INPUT_STREAM = PropertyService.class.getResourceAsStream(FILE);

    private static final Properties PROPERTIES = new Properties();

    static {

        try {
            PROPERTIES.load(INPUT_STREAM);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public Boolean getExternalServer() {
        return Boolean.parseBoolean(PROPERTIES.getProperty("jms.externalServer"));
    }





}
