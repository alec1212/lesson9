package com.galkin.jms;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.BasicConfigurator;

import javax.jms.*;

/**
 * Hello world!
 *
 */
public class AppRes
{
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;
    private static final String TOPIC = "MY_TOPIC";

    public static void main( String[] args ) throws JMSException {
        BasicConfigurator.configure();
        final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        final Destination destination = session.createTopic(TOPIC);
        final MessageConsumer messageConsumer = session.createConsumer(destination);
        final Message  message = messageConsumer.receive();
        System.out.println(String.format("+++++++++++++++ТЕСТ ++++++++++++++++++"));
        if (message instanceof TextMessage){
            TextMessage textMessage = (TextMessage) message;
            System.out.println(String.format("Получено: %s", textMessage.getText()));

        }
        connection.close();

    }
}
